﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StereogramGenerator
{
    public partial class Form1 : Form
    {
        private Bitmap Texture;
        private Bitmap DepthMap = new Bitmap(640, 480);
        private Bitmap Result = new Bitmap(640, 480, System.Drawing.Imaging.PixelFormat.Format32bppPArgb);
        private string Default = "C:Stereograms\\Result.jpg";
        private string TexturePath = "";
        private string DepthMapPath = "";

        public Form1()
        {
            InitializeComponent();
            textBox1.Text = Default;
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox3.SizeMode = PictureBoxSizeMode.StretchImage;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.ShowDialog();
            Image loadedImage = Image.FromFile(openFileDialog1.FileName);
            TexturePath = openFileDialog1.FileName;
            Texture = new Bitmap(loadedImage);
            pictureBox1.Image = Texture;
            pictureBox1.Refresh();
            button1.Visible = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.ShowDialog();
            DepthMapPath = openFileDialog1.FileName;

            Bitmap original = new Bitmap(openFileDialog1.FileName);
            DepthMap = new Bitmap(640, 480);

            using (Graphics gr = Graphics.FromImage(DepthMap))
            {
                gr.DrawImage(original, new Rectangle(0, 0, DepthMap.Width, DepthMap.Height));
            }

            pictureBox2.Image = DepthMap;
            pictureBox2.Refresh();
            button2.Visible = true;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            int width = 120, height = 98;

            //bitmap
            Bitmap bmp = new Bitmap(width, height);

            //random number
            Random rand = new Random();

            //create random pixels
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    //generate random ARGB value
                    int a = rand.Next(256);
                    int r = rand.Next(256);
                    int g = rand.Next(256);
                    int b = rand.Next(256);

                    //set ARGB value
                    bmp.SetPixel(x, y, Color.FromArgb(a, r, g, b));
                }
            }

            //load bmp in picturebox1
            pictureBox1.Image = bmp;
            Texture = bmp;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog openFileDialog1 = new FolderBrowserDialog();
            openFileDialog1.ShowDialog();
            textBox1.Text = openFileDialog1.SelectedPath + "\\Default.jpg";
        }

        private int mindepth(int maxdepth, int obsdist)
        {
            int mindepth = 0;
            double sepfactor = 0.55;

            mindepth = Convert.ToInt32((sepfactor * maxdepth * obsdist) / ((1 - sepfactor) * maxdepth + obsdist));
            return mindepth;
        }


        private void button4_Click(object sender, EventArgs e)
        {
            int patHeight = Texture.Height;

            const int maxwidth = 640 * 6;  // allow space for up to 6 times oversampling
            int xdpi = 75; int ydpi = 75;

            int yShift = ydpi / 16;

            int width = 640;
            int height = 480;

            int oversam = 4; // oversampling ratio
            int[] lookL = new int[maxwidth];
            int[] lookR = new int[maxwidth];
            Color[] colour = new Color[maxwidth];
            Color col;
            int lastlinked; int i;

            int vwidth = width * oversam;

            int obsDist = xdpi * 12;
            int eyeSep = Convert.ToInt32(xdpi * 2.5);
            int veyeSep = eyeSep * oversam;

            int maxdepth = xdpi * 12;
            int maxsep = (int)(((long)eyeSep * maxdepth) / (maxdepth + obsDist)); // pattern must be at
                                                                                  // least this wide : 94 pixels in this case
            int vmaxsep = oversam * maxsep;

            int featureZ, sep;
            int x, y, left, right;
            bool vis;

            sep = 0;

            for (y = 0; y < height; y++)
            {
                for (x = 0; x < vwidth; x++)
                {
                    lookL[x] = x;
                    lookR[x] = x;
                }

                for (x = 0; x < vwidth; x++)
                {
                    if ((x % oversam) == 0) // SPEEDUP for oversampled pictures
                    {
                        Color c = DepthMap.GetPixel(x / oversam, y);
                        int brightness = (int)Math.Round(c.GetBrightness() * 255.0);

                        featureZ = maxdepth - brightness * (maxdepth - mindepth(maxdepth, obsDist)) / 256;

                        sep = (int)(((long)veyeSep * featureZ) / (featureZ + obsDist));
                    }

                    left = x - sep / 2; right = left + sep;

                    vis = true;

                    if ((left >= 0) && (right < vwidth))
                    {
                        if (lookL[right] != right) // right pt already linked
                        {
                            if (lookL[right] < left) // deeper than current
                            {
                                lookR[lookL[right]] = lookL[right]; // break old links
                                lookL[right] = right;
                            }
                            else vis = false;
                        }

                        if (lookR[left] != left) // left pt already linked
                        {
                            if (lookR[left] > right) // deeper than current
                            {
                                lookL[lookR[left]] = lookR[left]; // break old links
                                lookR[left] = left;
                            }
                            else vis = false;
                        }
                        if (vis == true) { lookL[right] = left; lookR[left] = right; } // make link
                    }
                }

                lastlinked = -10; // dummy initial value

                for (x = 0; x < vwidth; x++)
                {
                    if (lookL[x] == x)
                    {
                        if (lastlinked == (x - 1)) colour[x] = colour[x - 1];
                        else
                        {
                            int newx = (x % vmaxsep) / oversam;
                            int newy = (y + (x / vmaxsep) * yShift) % patHeight;
                            colour[x] = Texture.GetPixel(newx, newy);
                        }
                    }
                    else
                    {
                        colour[x] = colour[lookL[x]];

                        lastlinked = x; // keep track of the last pixel to be constrained
                    }
                }

                int red, green, blue;

                for (x = 0; x < vwidth; x += oversam)
                {
                    red = 0; green = 0; blue = 0;

                    // use average colour of virtual pixels for screen pixel
                    for (i = x; i < (x + oversam); i++)
                    {
                        col = colour[i];
                        red += col.R;
                        green += col.G;
                        blue += col.B;
                    }
                    col = Color.FromArgb(red / oversam, green / oversam, blue / oversam);

                    Result.SetPixel(x / oversam, y, col);
                }
            }

            Result.Save(textBox1.Text, System.Drawing.Imaging.ImageFormat.Jpeg);

            pictureBox3.Image = Result;
            pictureBox3.Refresh();
        }
    }
}
