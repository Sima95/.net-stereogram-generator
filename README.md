This is a simple program to create Stereograms. It was made using C#. 

It does not have error handling yet and can only create Stereograms that are 640x480 pixels.

The algorithm used is described here: http://www.techmind.org/stereo/stech.html